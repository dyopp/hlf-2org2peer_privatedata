package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const assetCollection = "PropertyData"
const transferAgreementObjectType = "transferAgreement"

//creation of property transfer smart contract
type PropertyTransferSmartContract struct {
	contractapi.Contract
}

//Define property object
type Property struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Area      int    `json:"area"`
	OwnerName string `json:"ownerName"`
	Value     int    `json:"value"`
}

type PropertyPrivate struct {
	ID             string `json:"assetID"`
	AppraisedValue int    `json:"appraisedValue"`
}

type fullAssetProperty struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Area           int    `json:"area"`
	Value          int    `json:"value"`
	AppraisedValue int    `json:"appraisedValue"`
}

//Add Property to ledger
//smart contract instance as reciever(golang class kinda)
func (pc *PropertyTransferSmartContract) AddProperty(ctx contractapi.TransactionContextInterface) error {

	//Transient Data is private to the application/smartcontract interation. It is not recorded on the ledger.
	transientMap, err := ctx.GetStub().GetTransient()
	if err != nil {
		return fmt.Errorf("error getting transient: %v", err)
	}

	//pretty sure this comes from the ENV VAR ASSET_PROPERTIES
	transientAssetJSON, ok := transientMap["asset_properties"]
	if !ok {
		return fmt.Errorf("asset not found in the transient map input")
	}

	//Unmarshal transiant data
	var assetInput fullAssetProperty
	err = json.Unmarshal(transientAssetJSON, &assetInput)
	if err != nil {
		return fmt.Errorf("failed to unmarshal JSON: %v", err)
	}

	//Test unmarshalled data for completeness
	if len(assetInput.ID) == 0 {
		return fmt.Errorf("objectID field must be a non-empty string")
	}
	if len(assetInput.Name) == 0 {
		return fmt.Errorf("objectName field must be a non-empty string")
	}
	if assetInput.Area <= 0 {
		return fmt.Errorf("objectArea field must be positive int")
	}
	if assetInput.Value <= 0 {
		return fmt.Errorf("objectValue field must be positive int")
	}
	if assetInput.AppraisedValue <= 0 {
		return fmt.Errorf("objectAppraisedValue field must be a positive int")
	}

	//Check if current ID is already in use
	tempAsset, err := ctx.GetStub().GetPrivateData(assetCollection, assetInput.ID) //Not totally sure how assetCollection is used here, declared as a const
	if err != nil {
		return fmt.Errorf("failed to get asset on IDAlreadyInUse check: %v", err)
	} else if tempAsset != nil {
		fmt.Println("Asset already exists: " + assetInput.ID)
		return fmt.Errorf("this asset already exists and cannot be overwritten: " + assetInput.ID)
	}

	//Get ID of submitting client
	clientID, err := submittingClientIdentity(ctx)
	if err != nil {
		return fmt.Errorf("unable to get identity of submitting user: %v", err)
	}

	//Verify that client is submitting request to peer in their own organization
	//This is to ensure that outside client doesn't attempt to read/write private data on this peer
	err = verifyClientOrgMatchesPeerOrg(ctx)
	if err != nil {
		return fmt.Errorf("CrossRef Org; CreateAsset cannot be performed: Error %v", err)
	}

	clientName, _ := submittingClientIdentity(ctx)
	//Make full struct of core data
	asset := Property{
		ID:        assetInput.ID,
		Name:      assetInput.Name,
		Area:      assetInput.Area,
		OwnerName: clientName,
		Value:     assetInput.Value,
	}
	//Marshal object
	assetJSONasBytes, err := json.Marshal(asset)
	if err != nil {
		return fmt.Errorf("failed to marshal public data asset into JSON %v", err)
	}

	//Save core asset to private data collection
	log.Printf("CreatPublicAsset Put: collection %v, ID %v, owner %v", assetCollection, assetInput.ID, clientID) //assetCollection is used here again
	err = ctx.GetStub().PutPrivateData(assetCollection, assetInput.ID, assetJSONasBytes)
	if err != nil {
		return fmt.Errorf("failed to put asset into public data collection %v", err)
	}

	//If we want to keep data outside of a private data store
	// return ctx.GetStub().PutState(id, assetJSONasBytes) //pass json to api

	//Make struct of private data
	assetPrivateDetails := PropertyPrivate{
		ID:             assetInput.ID,
		AppraisedValue: assetInput.AppraisedValue,
	}
	//Marshal object
	assetPrivateDetailsAsBytes, err := json.Marshal(assetPrivateDetails)
	if err != nil {
		return fmt.Errorf("failed to marshal private data asset into JSON %v", err)
	}

	//Get collection name for this organization
	orgCollection, err := getCollectionName(ctx)
	if err != nil {
		return fmt.Errorf("failed to infer private collection name from the org: %v", err)
	}

	//Save private asset to private data collection
	log.Printf("CreatPrivateAsset Put: collection %v, ID %v, owner %v", assetCollection, assetInput.ID, clientID) //assetCollection is used here again
	err = ctx.GetStub().PutPrivateData(orgCollection, assetInput.ID, assetPrivateDetailsAsBytes)
	if err != nil {
		return fmt.Errorf("failed to put asset into private data collection %v", err)
	}

	//return ctx.GetStub().PutState(id, propertyBytes) //pass json to api
	return nil

}

//Query A Property
func (pc *PropertyTransferSmartContract) QueryAllProperties(ctx contractapi.TransactionContextInterface, assetID string) (*fullAssetProperty, error) {

	//Try to pull main asset
	log.Printf("ReadAsset: collection %v, ID %v", assetCollection, assetID)
	assetJSON, err := ctx.GetStub().GetPrivateData(assetCollection, assetID) //get the asset from chaincode state
	if err != nil {
		return nil, fmt.Errorf("failed to read asset: %v", err)
	}
	//If result is empty, return error
	if assetJSON == nil {
		log.Printf("Asset ID not found: %v; %v", assetID, assetCollection)
	}
	//Unmarshal main asset
	var asset *Property
	err = json.Unmarshal(assetJSON, &asset)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal JSON: %v", err)
	}
	//Get collection name for this organization
	orgCollection, err := getCollectionName(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to infer private collection name from the org%v", err)
	}
	//Check if private data exists
	log.Printf("ReadAsset: collection %v, ID %v", orgCollection, assetID)
	assetPrivateJSON, err := ctx.GetStub().GetPrivateData(orgCollection, assetID) //get the asset from chaincode state
	if err != nil {
		return nil, fmt.Errorf("failed to read asset: %v", err)
	}
	//Return public data if not private data found
	if assetPrivateJSON == nil {
		mergedProp := fullAssetProperty{
			ID:             asset.ID,
			Name:           asset.Name,
			Area:           asset.Area,
			Value:          asset.Value,
			AppraisedValue: 0,
		}
		return &mergedProp, nil //return just public data
	}
	//Unmarshal private data
	var privateAsset *PropertyPrivate
	err = json.Unmarshal(assetPrivateJSON, &privateAsset)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal JSON: %v", err)
	}
	//Private data needs to be merged with public data
	mergedProp := fullAssetProperty{
		ID:             asset.ID,
		Name:           asset.Name,
		Area:           asset.Area,
		Value:          asset.Value,
		AppraisedValue: privateAsset.AppraisedValue,
	}

	return &mergedProp, nil
}

// getCollectionName is an internal helper function to get collection of submitting client identity.
func getCollectionName(ctx contractapi.TransactionContextInterface) (string, error) {

	// Get the MSP ID of submitting client identity
	clientMSPID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return "", fmt.Errorf("failed to get verified MSPID: %v", err)
	}

	// Create the collection name
	orgCollection := clientMSPID + "PrivateCollection"

	return orgCollection, nil
}

// verifyClientOrgMatchesPeerOrg is an internal function used verify client org id and matches peer org id.
func verifyClientOrgMatchesPeerOrg(ctx contractapi.TransactionContextInterface) error {
	clientMSPID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("failed getting the client's MSPID: %v", err)
	}
	peerMSPID, err := shim.GetMSPID()
	if err != nil {
		return fmt.Errorf("failed getting the peer's MSPID: %v", err)
	}

	if clientMSPID != peerMSPID {
		return fmt.Errorf("client from org %v is not authorized to read or write private data from an org %v peer", clientMSPID, peerMSPID)
	}

	return nil
}

func submittingClientIdentity(ctx contractapi.TransactionContextInterface) (string, error) {
	b64ID, err := ctx.GetClientIdentity().GetID()
	if err != nil {
		return "", fmt.Errorf("failed to read clientID: %v", err)
	}
	decodeID, err := base64.StdEncoding.DecodeString(b64ID)
	if err != nil {
		return "", fmt.Errorf("failed to base64 decode clientID: %v", err)
	}
	return string(decodeID), nil
}

func main() {
	//new instance of smart contract struct
	propTransferSmartContract := new(PropertyTransferSmartContract)
	//Add chain code
	cc, err := contractapi.NewChaincode(propTransferSmartContract)
	if err != nil {
		panic(err.Error())
	}
	//Init chain code
	if err := cc.Start(); err != nil {
		panic(err.Error())
	}
}

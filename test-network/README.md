### Build chaincode
>   cd chaincode/Property_app/Go
>   go mod init example.com/property

>   go get

>   go build



# Setup the network
>   cd ../../../test-network
###     Create peers
>   ./network.sh up -ca -s couchdb
###     Setup channel
>   ./network.sh createChannel
###     Deploy chaincode on peers
>   ./network.sh deployCC -ccn private -ccp ../chaincode/Property_app/Go -ccl go -ccep "OR('Org1MSP.peer','Org2MSP.peer')" -cccg ../chaincode/Property_app/collections_config.json


# Register Identities for Each Org
###    Set ENV Vars
>	export PATH=${PWD}/../bin:${PWD}:$PATH

>	export FABRIC_CFG_PATH=$PWD/../config/

###     Create "owner" as part of Org1
>   export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/org1.example.com/

>   fabric-ca-client register --caname ca-org1 --id.name owner --id.secret ownerpw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"

>   fabric-ca-client enroll -u https://owner:ownerpw@localhost:7054 --caname ca-org1 -M "${PWD}/organizations/peerOrganizations/org1.example.com/users/owner@org1.example.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"

>   cp "${PWD}/organizations/peerOrganizations/org1.example.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org1.example.com/users/owner@org1.example.com/msp/config.yaml"

###     Create "buyer" as part of Org2
>	export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/org2.example.com/

>	fabric-ca-client register --caname ca-org2 --id.name buyer --id.secret buyerpw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"

>	fabric-ca-client enroll -u https://buyer:buyerpw@localhost:8054 --caname ca-org2 -M "${PWD}/organizations/peerOrganizations/org2.example.com/users/buyer@org2.example.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"

>	cp "${PWD}/organizations/peerOrganizations/org2.example.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org2.example.com/users/buyer@org2.example.com/msp/config.yaml"


# Use Identites to Create and Query Chaincode
### Create a property as "owner" Identity(org1)

Set ENV VARs to buyer/Org2
>   export PATH=${PWD}/../bin:$PATH

>   export FABRIC_CFG_PATH=$PWD/../config/

>   export CORE_PEER_TLS_ENABLED=true

>   export CORE_PEER_LOCALMSPID="Org1MSP"

>   export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt

>   export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/owner@org1.example.com/msp

>   export CORE_PEER_ADDRESS=localhost:7051

Set Transient data into an ENV VAR
>   export ASSET_PROPERTIES=$(echo -n "{\"objectType\":\"property\",\"id\":\"1\",\"name\":\"south main street\",\"area\":2000,\"value\":255000,\"appraisedValue\":300000}" | base64 | tr -d \\n)

Call chaincode to create a new Property using transient data
>   peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" -C mychannel -n private -c '{"function":"AddProperty","Args":[]}' --transient "{\"asset_properties\":\"$ASSET_PROPERTIES\"}"

### Query property as "owner" Identity(org1)
Query the property we just created
>   peer chaincode query -C mychannel -n private -c '{"function":"QueryAllProperties","Args":["1"]}'

### Query property "buyer" Identity(org2)

Set ENV VARs to buyer/Org2
>   export CORE_PEER_LOCALMSPID="Org2MSP"

>   export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt

>   export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/buyer@org2.example.com/msp

>   export CORE_PEER_ADDRESS=localhost:9051

Query the property that was created by org 1
Notice that appaised value is withheld

>   peer chaincode query -C mychannel -n private -c '{"function":"QueryAllProperties","Args":["1"]}'
